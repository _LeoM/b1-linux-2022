<br>

# **TP 5 : Self-hosted cloud**

<br>

## **Partie 1 : Mise en place et maîtrise du serveur Web**

<br>

### **1. Installation**

<br>

🌞 Installer le serveur Apache

```bash
[lm33@web ~]$ sudo dnf install httpd

...

Complete!
```

```bash
[lm33@web conf]$ cat httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
```

🌞 Démarrer le service Apache

```bash
[lm33@web conf]$ sudo systemctl start httpd
[lm33@web conf]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 14:56:37 CET; 5s ago
       Docs: man:httpd.service(8)
   Main PID: 1342 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5906)
     Memory: 23.1M
        CPU: 54ms
     CGroup: /system.slice/httpd.service
             ├─1342 /usr/sbin/httpd -DFOREGROUND
             ├─1343 /usr/sbin/httpd -DFOREGROUND
             ├─1344 /usr/sbin/httpd -DFOREGROUND
             ├─1345 /usr/sbin/httpd -DFOREGROUND
             └─1346 /usr/sbin/httpd -DFOREGROUND

Jan 03 14:56:37 web systemd[1]: Starting The Apache HTTP Server...
Jan 03 14:56:37 web httpd[1342]: AH00558: httpd: Could not reliably determine the server's fully qual>
Jan 03 14:56:37 web systemd[1]: Started The Apache HTTP Server.
Jan 03 14:56:37 web httpd[1342]: Server configured, listening on: port 80
```

```bash
[lm33@web conf]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```

```bash
[lm33@web conf]$ sudo ss -alpnt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1346,fd=4),("httpd",pid=1345,fd=4),("httpd",pid=1344,fd=4),("httpd",pid=1342,fd=4))
```

```bash
[lm33@web conf]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[lm33@web conf]$ sudo firewall-cmd --reload
success
[lm33@web conf]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 TEST

```bash
[lm33@web conf]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 14:56:37 CET; 7min ago
       Docs: man:httpd.service(8)
   Main PID: 1342 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5906)
     Memory: 23.1M
        CPU: 215ms
     CGroup: /system.slice/httpd.service
             ├─1342 /usr/sbin/httpd -DFOREGROUND
             ├─1343 /usr/sbin/httpd -DFOREGROUND
             ├─1344 /usr/sbin/httpd -DFOREGROUND
             ├─1345 /usr/sbin/httpd -DFOREGROUND
             └─1346 /usr/sbin/httpd -DFOREGROUND

Jan 03 14:56:37 web systemd[1]: Starting The Apache HTTP Server...
Jan 03 14:56:37 web httpd[1342]: AH00558: httpd: Could not reliably determine the server's fully qual>
Jan 03 14:56:37 web systemd[1]: Started The Apache HTTP Server.
Jan 03 14:56:37 web httpd[1342]: Server configured, listening on: port 80
```

```bash
[lm33@web conf]$ sudo systemctl is-enabled httpd
enabled
```

```bash
[lm33@web conf]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
...
```

```bash
PS C:\Users\macel> curl 10.4.1.249:80
curl : HTTP Server Test Page
...
```

<br>

### **2. Avancer vers la maîtrise du service**

<br>

🌞 Le service Apache...

```bash
[lm33@web httpd]$ sudo cat /usr/lib/systemd/system/httpd.service
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 Déterminer sous quel utilisateur tourne le processus Apache

```bash
[lm33@web httpd]$ cat conf/httpd.conf | grep -i user
User apache
```

```bash
[lm33@web httpd]$ ps -ef | grep httpd
root        1342       1  0 14:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1343    1342  0 14:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1344    1342  0 14:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1345    1342  0 14:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1346    1342  0 14:56 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
lm33        1693     974  0 15:20 pts/0    00:00:00 grep --color=auto httpd
```

```bash
[lm33@web httpd]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Jan  3 14:46 .
drwxr-xr-x. 82 root root 4096 Jan  3 14:46 ..
-rw-r--r--.  1 root root 7620 Jul 27 20:05 index.html
```

🌞 Changer l'utilisateur utilisé par Apache

```bash
[lm33@web httpd]$ sudo useradd web -m -s /sbin/nologin
[lm33@web httpd]$ sudo usermod -d /usr/share/httpd web
[lm33@web httpd]$ cat /etc/passwd
...
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
web:x:1001:1001::/usr/share/httpd:/sbin/nologin
```

```bash
[lm33@web httpd]$ cat conf/httpd.conf | grep -i user
User web
```

```bash
[lm33@web httpd]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 15:39:48 CET; 6s ago
       Docs: man:httpd.service(8)
   Main PID: 1986 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5906)
     Memory: 22.7M
        CPU: 51ms
     CGroup: /system.slice/httpd.service
             ├─1986 /usr/sbin/httpd -DFOREGROUND
             ├─1987 /usr/sbin/httpd -DFOREGROUND
             ├─1988 /usr/sbin/httpd -DFOREGROUND
             ├─1989 /usr/sbin/httpd -DFOREGROUND
             └─1990 /usr/sbin/httpd -DFOREGROUND

Jan 03 15:39:48 web systemd[1]: Starting The Apache HTTP Server...
Jan 03 15:39:48 web httpd[1986]: AH00558: httpd: Could not reliably determine the server's fully qual>
Jan 03 15:39:48 web systemd[1]: Started The Apache HTTP Server.
Jan 03 15:39:48 web httpd[1986]: Server configured, listening on: port 80

[lm33@web httpd]$ ps -ef | grep httpd
root        1986       1  0 15:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1987    1986  0 15:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1988    1986  0 15:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1989    1986  0 15:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web         1990    1986  0 15:39 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
lm33        2209     974  0 15:42 pts/0    00:00:00 grep --color=auto httpd
```

🌞 Faites en sorte que Apache tourne sur un autre port

```bash
[lm33@web httpd]$ cat conf/httpd.conf | grep -i listen
Listen 8082
[lm33@web httpd]$ sudo systemctl restart httpd
[lm33@web httpd]$ sudo firewall-cmd --add-port=8082/tcp --permanent
success
[lm33@web httpd]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[lm33@web httpd]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8082/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[lm33@web httpd]$ sudo firewall-cmd --reload
success
```

```grep
[lm33@web httpd]$ sudo ss -lapnt | grep httpd
LISTEN 0      511                *:8082            *:*     users:(("httpd",pid=2294,fd=4),("httpd",pid=2293,fd=4),("httpd",pid=2292,fd=4),("httpd",pid=2288,fd=4))
```

```grep
[lm33@web httpd]$ curl 10.4.1.249:8082
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
...
```

📁 Fichier [/etc/httpd/conf/httpd.conf](/files/httpd.conf)

<br>

## **Partie 2 : Mise en place et maîtrise du serveur de base de données**

<br>

🌞 Install de MariaDB sur db.tp5.linux


```bash
[lm33@db ~]$ sudo dnf install mariadb-server
[sudo] password for lm33:
Last metadata expiration check: 0:34:40 ago on Tue 03 Jan 2023 03:43:12 PM CET.
Dependencies resolved.
...

Complete!
```

```bash
[lm33@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[lm33@db ~]$ sudo systemctl start mariadb
[lm33@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-03 16:21:06 CET; 13s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
    Process: 12822 ExecStartPre=/usr/libexec/mariadb-check-socket (code=exited, status=0/SUCCESS)
    Process: 12844 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir mariadb.service (code=exited, sta>
    Process: 12937 ExecStartPost=/usr/libexec/mariadb-check-upgrade (code=exited, status=0/SUCCESS)
   Main PID: 12925 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 11 (limit: 5906)
     Memory: 73.8M
        CPU: 439ms
     CGroup: /system.slice/mariadb.service
             └─12925 /usr/libexec/mariadbd --basedir=/usr

Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: you need to be the system 'mysql' user to connect.
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: After connecting you can set the password, if you w>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: able to connect as any of these users with a passwo>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: See the MariaDB Knowledgebase at https://mariadb.co>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: Please report any problems at https://mariadb.org/j>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: The latest information about MariaDB is available a>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: Consider joining MariaDB's strong and vibrant commu>
Jan 03 16:21:06 db mariadb-prepare-db-dir[12883]: https://mariadb.org/get-involved/
Jan 03 16:21:06 db mariadbd[12925]: 2023-01-03 16:21:06 0 [Note] /usr/libexec/mariadbd (mysqld 10.5.1>
Jan 03 16:21:06 db systemd[1]: Started MariaDB 10.5 database server.
```

🌞 Port utilisé par MariaDB

 ```bash
 [lm33@db ~]$ sudo ss -lapnt | grep maria
LISTEN 0      80                 *:3306            *:*     users:(("mariadbd",pid=12925,fd=15))  
```

```bash
[lm33@db ~]$ sudo firewall-cmd --add-port=3306/tcp
success
[lm33@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[lm33@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[lm33@db ~]$ sudo firewall-cmd --reload
success
```

🌞 Processus liés à MariaDB

```bash
[lm33@db ~]$ ps -ef | grep maria
mysql      12925       1  0 16:21 ?        00:00:00 /usr/libexec/mariadbd --basedir=/usr
```

<br>

## **Partie 3 : Configuration et mise en place de NextCloud**

<br>

### **1. Base de données**

🌞 Préparation de la base pour NextCloud

```sql
[lm33@db ~]$ sudo mysql -u root -p
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.4.1.249' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.4.1.249';
Query OK, 0 rows affected (0.013 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 Exploration de la base de données


```bash
[lm33@web httpd]$ sudo dnf install mysql
```

```bash
[lm33@web httpd]$ mysql -u nextcloud -h 10.4.1.250 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 23
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```


```sql
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```

🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données

```sql
[lm33@db ~]$ sudo mysql
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 29
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT User FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
5 rows in set (0.001 sec)

MariaDB [(none)]>
```

### **2. Serveur Web et NextCloud**

🌞 Install de PHP

```bash
[lm33@web httpd]$ sudo dnf config-manager --set-enabled crb
[lm33@web httpd]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
...
Complete!
```

```bash
[lm33@web httpd]$ dnf module list php
```

```bash
[lm33@web httpd]$ sudo dnf module enable php:remi-8.1 -y
...
Complete!
```

```bash
[lm33@web httpd]$ sudo dnf install -y php81-php
...
Complete!
```

🌞 Install de tous les modules PHP nécessaires pour NextCloud

```bash
[lm33@web httpd]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
...
Complete!
```

🌞 Récupérer NextCloud

```bash
[lm33@web www]$ sudo mkdir tp5_nextcloud
```

```bash
[lm33@web ~]$ mkdir ouais
[lm33@web ~]$ cd ouais
[lm33@web ouais]$ curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output -ouais
[lm33@web ~]$ unzip ouais
...
```

```bash
[lm33@web ouais]$ sudo mv nextcloud /var/www/tp5_nextcloud/
```

```bash
[lm33@web nextcloud]$ sudo mv * /var/www/tp5_nextcloud/
[lm33@web nextcloud]$ sudo mv .htaccess /var/www/tp5_nextcloud/
[lm33@web nextcloud]$ sudo mv .user.ini /var/www/tp5_nextcloud/
```

```bash
[lm33@web www]$ sudo chown -R apache:apache tp5_nextcloud/
[lm33@web tp5_nextcloud]$ ls -al
total 140
drwxr-xr-x. 14 apache apache  4096 Jan  9 09:12 .
drwxr-xr-x.  5 root   root      54 Jan  3 17:38 ..
drwxr-xr-x. 47 apache apache  4096 Oct  6 14:47 3rdparty
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache apache    67 Oct  6 14:47 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache apache  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache apache  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache apache  3253 Oct  6 14:42 .htaccess
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:42 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache apache   101 Oct  6 14:42 .user.ini
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
```

🌞 Adapter la configuration d'Apache


```bash
[lm33@web conf.d]$ sudo nano tp5_conf.conf
[lm33@web tp5_nextcloud]$ cat /etc/httpd/conf.d/tp5_conf.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 Redémarrer le service Apache

```bash
[lm33@web ~]$ sudo systemctl restart httpd
```

### **3. Finaliser l'installation de NextCloud**

🌞 Exploration de la base de données

```bash
[lm33@db ~]$ sudo mysql -u root -p
```

```sql
MariaDB [nextcloud]> select count(*) as NumberOfTableInNextCloud from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'ne
xtcloud';
+--------------------------+
| NumberOfTableInNextCloud |
+--------------------------+
|                       95 |
+--------------------------+
1 row in set (0.000 sec)
```
