#!/bin/bash

dnf install httpd
systemctl start httpd
sudo systemctl enable httpd
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --reload


dnf install mysql

# Partie du user partie 3 faire

dnf config-manager --set-enabled crb
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
sudo dnf module enable php:remi-8.1 -y
dnf install -y php81-php
dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
mkdir /var/www/tp5_nextcloud
mkdir ouais
cd ouais
curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output -ouais
unzip ouais
mv nextcloud /var/www/tp5_nextcloud/
mv /var/www/tp5_nextcloud/* /var/www/tp5_nextcloud/
mv /var/www/tp5_nextcloud/.htaccess /var/www/tp5_nextcloud/
mv /var/www/tp5_nextcloud/.user.ini /var/www/tp5_nextcloud/
chown -R apache:apache /var/www/tp5_nextcloud/

echo "<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>" > /etc/httpd/conf.d/tp5_conf.conf

systemctl restart httpd
