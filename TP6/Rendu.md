<br>

# **TP6 : **

<br>

## **Partie 1 : Reverse Proxy**

### **I. Setup**

🌞 On utilisera NGINX comme reverse proxy

```bash
[lm33@proxy ~]$ sudo dnf install nginx
...
Complete!

[lm33@proxy ~]$ sudo systemctl start nginx
[lm33@proxy ~]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-16 09:58:47 CET; 5s ago
    Process: 1214 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1215 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1216 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1217 (nginx)
      Tasks: 2 (limit: 5906)
     Memory: 1.9M
        CPU: 15ms
     CGroup: /system.slice/nginx.service
             ├─1217 "nginx: master process /usr/sbin/nginx"
             └─1218 "nginx: worker process"

Jan 16 09:58:47 proxy systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jan 16 09:58:47 proxy nginx[1215]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Jan 16 09:58:47 proxy nginx[1215]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Jan 16 09:58:47 proxy systemd[1]: Started The nginx HTTP and reverse proxy server.

[lm33@proxy ~]$ sudo ss -lapnt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*     users:(("nginx",pid=1218,fd=6),("nginx",pid=1217,fd=6))
LISTEN 0      511             [::]:80           [::]:*     users:(("nginx",pid=1218,fd=7),("nginx",pid=1217,fd=7))

[lm33@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success

[lm33@proxy ~]$ sudo firewall-cmd --reload
success
[lm33@proxy ~]$ sudo firewall-cmd --list-all | grep port
  ports: 80/tcp
  forward-ports:
  source-ports:

[lm33@proxy ~]$ ps -ef | grep nginx
root        1217       1  0 09:58 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1218    1217  0 09:58 ?        00:00:00 nginx: worker process
lm33        1306    1032  0 10:03 pts/0    00:00:00 grep --color=auto nginx
```

```powershell
PS C:\Users\macel> curl 10.4.1.251:80


StatusCode        : 200
StatusDescription : OK
Content           : <!doctype html>
                    <html>
                      <head>
                        <meta charset='utf-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1'>
                        <title>HTTP Server Test Page powered by: Rocky Linux</title>
```

🌞 Configurer NGINX

```bash
[lm33@proxy ~]$ sudo nano /etc/nginx/conf.d/tp6proxy.conf
[lm33@proxy ~]$ cat /etc/nginx/conf.d/tp6proxy.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.linux.tp5;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.4.1.249:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}

[lm33@web ~]$ sudo nano /var/www/tp5_nextcloud/config/config.php
[lm33@web ~]$ sudo cat /var/www/tp5_nextcloud/config/config.php
<?php
$CONFIG = array (
  'instanceid' => 'ocdi4e5dornu',
  'passwordsalt' => 'zkMZeXs+LNgznO4L+Nbj75CvkGNW2O',
  'secret' => 'RzPtycOCirVW0vdaT8KMQXn48uvUVbKq3CcdD1xEb0NtKm1K',
  'trusted_domains' =>
  array (
    0 => '10.4.1.251',
  ),
  'datadirectory' => '/var/www/tp5_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://10.4.1.249',
  'dbname' => 'nextcloud',
  'dbhost' => '10.4.1.250:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'pewpewpew',
  'installed' => true,
);
```

🌞 Faites en sorte de

```bash
[lm33@web ~]$ sudo firewall-cmd --set-default-zone drop
success
[lm33@web ~]$ sudo firewall-cmd --permanent --zone=drop --add-source=10.4.1.251
success
[lm33@web ~]$ sudo firewall-cmd --permanent --zone=drop --add-source=10.4.1.251/24
success
[lm33@web ~]$ sudo firewall-cmd --reload
success
[lm33@web ~]$ sudo firewall-cmd --list-all
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 10.4.1.251 10.4.1.251/24
  services:
  ports:
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[lm33@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
  sources: 10.4.1.251 10.4.1.251/24
```

🌞 Une fois que c'est en place

```powershell
PS C:\Users\macel> ping 10.4.1.249

Envoi d’une requête 'Ping'  10.4.1.249 avec 32 octets de données :
Délai d’attente de la demande dépassé.

Statistiques Ping pour 10.4.1.249:
    Paquets : envoyés = 1, reçus = 0, perdus = 1 (perte 100%),
```

                                                                              IL MANQUE CETTE PARTIE

## **Partie 2 : Sauvegarde du système de fichiers**

### **I. Script de backup**

**1. Ecriture du script**

🌞 Ecrire le script bash










## **Partie 3 : Fail2Ban**

🌞 Faites en sorte que :

```bash
[lm33@db ~]$ sudo dnf install epel-release -y
...
Complete!
[lm33@db ~]$ sudo dnf install fail2ban -y
...
Complete!

[lm33@db ~]$ sudo vim /etc/fail2ban/jail.conf
[lm33@db ~]$ sudo cat /etc/fail2ban/jail.conf


[INCLUDES]

before = paths-fedora.conf


[DEFAULT]

ignorecommand =

bantime  = 10m

findtime  = 1m

maxretry = 3
...

[lm33@db ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[lm33@db ~]$ sudo systemctl start fail2ban
[lm33@db ~]$ systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-16 11:58:48 CET; 9s ago
       Docs: man:fail2ban(1)
    Process: 12469 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 12470 (fail2ban-server)
      Tasks: 3 (limit: 5906)
     Memory: 12.3M
        CPU: 86ms
     CGroup: /system.slice/fail2ban.service
             └─12470 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Jan 16 11:58:48 db systemd[1]: Starting Fail2Ban Service...
Jan 16 11:58:48 db systemd[1]: Started Fail2Ban Service.
Jan 16 11:58:48 db fail2ban-server[12470]: 2023-01-16 11:58:48,107 fail2ban.configreader   [12470]: W>
Jan 16 11:58:48 db fail2ban-server[12470]: Server ready
lines 1-16/16 (END)

[lm33@web ~]$ ssh lm33@10.4.1.250
Permission denied, please try again.
Permission denied, please try again.
lm33@10.4.1.250: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[lm33@web ~]$ ssh lm33@10.4.1.250
ssh: connect to host 10.4.1.250 port 22: Connection refused

[lm33@db ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
[lm33@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     0
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     0
   `- Banned IP list:
[lm33@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.4.1.249

[lm33@db ~]$ sudo iptables -L | grep REJECT
REJECT     all  --  10.4.1.249          anywhere             reject-with icmp-port-unreachable

[lm33@db ~]$ sudo fail2ban-client set sshd unbanip 10.4.1.249
0
[lm33@db ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```

## **Partie 4 : Monitoring**

🌞 Installer Netdata

```bash
[lm33@db ~]$ sudo dnf install epel-release -y
Complete!

[lm33@web ~]$ sudo dnf install epel-release -y
Complete!

[lm33@web ~] wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh

[lm33@db ~] wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh

[lm33@db ~]$ sudo systemctl start netdata
[lm33@db ~]$ sudo systemctl enable netdata
[lm33@db ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-17 14:59:12 CET; 37s ago
   Main PID: 40915 (netdata)
      Tasks: 77 (limit: 5906)
     Memory: 129.9M
        CPU: 2.428s
     CGroup: /system.slice/netdata.service
             ├─40915 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─40917 /usr/sbin/netdata --special-spawn-server
             ├─41096 /usr/libexec/netdata/plugins.d/apps.plugin 1
             ├─41098 /usr/libexec/netdata/plugins.d/go.d.plugin 1
             ├─41101 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
             └─41102 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1

[lm33@web ~]$ sudo systemctl start netdata
[lm33@web ~]$ sudo systemctl enable netdata
[lm33@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2023-01-17 14:58:28 CET; 2min 4s ago
   Main PID: 45469 (netdata)
      Tasks: 77 (limit: 5906)
     Memory: 138.7M
        CPU: 3.678s
     CGroup: /system.slice/netdata.service
             ├─45469 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─45473 /usr/sbin/netdata --special-spawn-server
             ├─45649 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─45654 /usr/libexec/netdata/plugins.d/apps.plugin 1
             ├─45656 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
             └─45657 /usr/libexec/netdata/plugins.d/go.d.plugin 1

[lm33@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[lm33@web ~]$ sudo firewall-cmd --reload
success
[lm33@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp 19999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[lm33@db ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[lm33@db ~]$ sudo firewall-cmd --reload
success
[lm33@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp 19999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

[lm33@web ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=45469,fd=41))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=45469,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=45469,fd=40))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=45469,fd=7))

[lm33@db ~]$ sudo ss -alpnt | grep netdata
LISTEN 0      4096       127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=40915,fd=42))
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=40915,fd=6))
LISTEN 0      4096           [::1]:8125          [::]:*    users:(("netdata",pid=40915,fd=41))
LISTEN 0      4096            [::]:19999         [::]:*    users:(("netdata",pid=40915,fd=7))
```

🌞 Une fois Netdata installé et fonctionnel, déterminer :

```bash
[lm33@web ~]$ sudo ps -ef | grep netdata
root       45230       1  0 14:58 ?        00:00:00 gpg-agent --homedir /var/cache/dnf/netdata-edge-a383c484584e0b14/pubring --use-standard-socket --daemon
root       45232   45230  0 14:58 ?        00:00:00 scdaemon --multi-server --homedir /var/cache/dnf/netdata-edge-a383c484584e0b14/pubring
root       45273       1  0 14:58 ?        00:00:00 gpg-agent --homedir /var/cache/dnf/netdata-repoconfig-3ca68ffb39611f32/pubring --use-standard-socket --daemon
root       45275   45273  0 14:58 ?        00:00:00 scdaemon --multi-server --homedir /var/cache/dnf/netdata-repoconfig-3ca68ffb39611f32/pubring
netdata    45469       1  0 14:58 ?        00:00:04 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
netdata    45473   45469  0 14:58 ?        00:00:00 /usr/sbin/netdata --special-spawn-server
netdata    45649   45469  0 14:58 ?        00:00:00 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
netdata    45654   45469  0 14:58 ?        00:00:02 /usr/libexec/netdata/plugins.d/apps.plugin 1
root       45656   45469  0 14:58 ?        00:00:00 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
netdata    45657   45469  0 14:58 ?        00:00:01 /usr/libexec/netdata/plugins.d/go.d.plugin 1
lm33       46140    1110  0 15:06 pts/0    00:00:00 grep --color=auto netdata

[lm33@db ~]$ sudo ps -ef | grep netdata
root       40677       1  0 14:58 ?        00:00:00 gpg-agent --homedir /var/cache/dnf/netdata-edge-a383c484584e0b14/pubring --use-standard-socket --daemon
root       40679   40677  0 14:58 ?        00:00:00 scdaemon --multi-server --homedir /var/cache/dnf/netdata-edge-a383c484584e0b14/pubring
root       40720       1  0 14:58 ?        00:00:00 gpg-agent --homedir /var/cache/dnf/netdata-repoconfig-3ca68ffb39611f32/pubring --use-standard-socket --daemon
root       40722   40720  0 14:58 ?        00:00:00 scdaemon --multi-server --homedir /var/cache/dnf/netdata-repoconfig-3ca68ffb39611f32/pubring
netdata    40915       1  0 14:59 ?        00:00:04 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
netdata    40917   40915  0 14:59 ?        00:00:00 /usr/sbin/netdata --special-spawn-server
netdata    41096   40915  0 14:59 ?        00:00:02 /usr/libexec/netdata/plugins.d/apps.plugin 1
netdata    41098   40915  0 14:59 ?        00:00:01 /usr/libexec/netdata/plugins.d/go.d.plugin 1
root       41101   40915  0 14:59 ?        00:00:00 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
netdata    41102   40915  0 14:59 ?        00:00:00 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
lm33       41524     997  0 15:08 pts/0    00:00:00 grep --color=auto netdata

[lm33@db ~]$ ls -al /var/log/netdata/
```

🌞 Configurer Netdata pour qu'il vous envoie des alertes

```bash
[lm33@db ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
[lm33@db ~]$ cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1064912327914311771/QpyPgu-lYyQ___vqvk4MyLoGdz19U6BhrasIxWyvtXeiXat8XhDlyRXZtXL8X7FW0581"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

[lm33@web ~]$ sudo nano /etc/netdata/health_alarm_notify.conf
[lm33@web ~]$ cat /etc/netdata/health_alarm_notify.conf
###############################################################################
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1064912327914311771/QpyPgu-lYyQ___vqvk4MyLoGdz19U6BhrasIxWyvtXeiXat8XhDlyRXZtXL8X7FW0581"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
```

🌞 Vérifier que les alertes fonctionnent

```bash
[lm33@db ~]$ sudo dnf install stress
Complete!
[lm33@db ~]$ stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 120s
stress: info: [42429] dispatching hogs: 8 cpu, 4 io, 2 vm, 0 hdd
```