<br>

# **TP2 : Appréhender l'environnement Linux**

<br>

## **I. Service SSH**

<br>

### **1. Analyse du service**

<br>

🌞 S'assurer que le service sshd est démarré

```bash
[lm33@machine1 ~]$ systemctl status sshd

● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-12-05 11:17:43 CET; 4min 8s ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 689 (sshd)
      Tasks: 1 (limit: 5906)
     Memory: 5.6M
        CPU: 87ms
     CGroup: /system.slice/sshd.service
             └─689 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Dec 05 11:17:43 localhost systemd[1]: Starting OpenSSH server daemon...
Dec 05 11:17:43 localhost sshd[689]: Server listening on 0.0.0.0 port 22.
Dec 05 11:17:43 localhost sshd[689]: Server listening on :: port 22.
Dec 05 11:17:43 localhost systemd[1]: Started OpenSSH server daemon.
Dec 05 11:18:11 localhost.localdomain sshd[863]: Accepted password for lm33 from 10.3.1.3 port 60440 >
Dec 05 11:18:11 localhost.localdomain sshd[863]: pam_unix(sshd:session): session opened for user lm33>
Dec 05 11:18:22 machine1 sshd[890]: Accepted password for lm33 from 10.3.1.3 port 60442 ssh2
Dec 05 11:18:22 machine1 sshd[890]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by>
lines 1-20/20 (END)
```

🌞 Analyser les processus liés au service SSH

```bash
[lm33@machine1 ~]$ ps -ef | grep sshd

root         689       1  0 11:17 ?        00:00:00 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
root         890     689  0 11:18 ?        00:00:00 sshd: lm33 [priv]
lm33         894     890  0 11:18 ?        00:00:00 sshd: lm33@pts/0
lm33         934     895  0 11:23 pts/0    00:00:00 grep --color=auto sshd
```

🌞 Déterminer le port sur lequel écoute le service SSH

```bash
[lm33@machine1 ~]$ sudo ss -alnpt | grep sshd
LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=689,fd=3))
LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=689,fd=4))
```

Le service SSH écoute sur le port 22.

<br>

🌞 Consulter les logs du service SSH

```bash
[lm33@machine1 ~]$ journalctl | grep ssh
Dec 05 11:17:42 localhost systemd[1]: Created slice Slice /system/sshd-keygen.
Dec 05 11:17:42 localhost systemd[1]: Reached target sshd-keygen.target.
Dec 05 11:17:43 localhost sshd[689]: Server listening on 0.0.0.0 port 22.
Dec 05 11:17:43 localhost sshd[689]: Server listening on :: port 22.
Dec 05 11:18:11 localhost.localdomain sshd[863]: Accepted password for lm33 from 10.3.1.3 port 60440 ssh2
Dec 05 11:18:11 localhost.localdomain sshd[863]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by (uid=0)
Dec 05 11:18:19 machine1 sshd[867]: Received disconnect from 10.3.1.3 port 60440:11: disconnected by user
Dec 05 11:18:19 machine1 sshd[867]: Disconnected from user lm33 10.3.1.3 port 60440
Dec 05 11:18:19 machine1 sshd[863]: pam_unix(sshd:session): session closed for user lm33
Dec 05 11:18:22 machine1 sshd[890]: Accepted password for lm33 from 10.3.1.3 port 60442 ssh2
Dec 05 11:18:22 machine1 sshd[890]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by (uid=0)
```


```bash
[lm33@machine1 ~]$ sudo cat /var/log/secure | grep sshd
Oct 14 11:39:26 localhost sshd[747]: Server listening on 0.0.0.0 port 22.
Oct 14 11:39:26 localhost sshd[747]: Server listening on :: port 22.
Oct 14 11:42:02 localhost sshd[747]: Received signal 15; terminating.
Oct 14 11:42:02 localhost sshd[29565]: Server listening on 0.0.0.0 port 22.
Oct 14 11:42:02 localhost sshd[29565]: Server listening on :: port 22.
Dec  5 11:08:13 localhost sshd[692]: Server listening on 0.0.0.0 port 22.
Dec  5 11:08:13 localhost sshd[692]: Server listening on :: port 22.
Dec  5 11:15:32 localhost sshd[1009]: Accepted password for lm33 from 10.3.1.3 port 60430 ssh2
Dec  5 11:15:32 localhost sshd[1009]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by (uid=0)
Dec  5 11:16:37 localhost sshd[1013]: Received disconnect from 10.3.1.3 port 60430:11: disconnected by user
Dec  5 11:16:37 localhost sshd[1013]: Disconnected from user lm33 10.3.1.3 port 60430
Dec  5 11:16:37 localhost sshd[1009]: pam_unix(sshd:session): session closed for user lm33
Dec  5 11:17:43 localhost sshd[689]: Server listening on 0.0.0.0 port 22.
Dec  5 11:17:43 localhost sshd[689]: Server listening on :: port 22.
Dec  5 11:18:11 localhost sshd[863]: Accepted password for lm33 from 10.3.1.3 port 60440 ssh2
Dec  5 11:18:11 localhost sshd[863]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by (uid=0)
Dec  5 11:18:19 localhost sshd[867]: Received disconnect from 10.3.1.3 port 60440:11: disconnected by user
Dec  5 11:18:19 localhost sshd[867]: Disconnected from user lm33 10.3.1.3 port 60440
Dec  5 11:18:19 localhost sshd[863]: pam_unix(sshd:session): session closed for user lm33
Dec  5 11:18:22 localhost sshd[890]: Accepted password for lm33 from 10.3.1.3 port 60442 ssh2
Dec  5 11:18:22 localhost sshd[890]: pam_unix(sshd:session): session opened for user lm33(uid=1000) by (uid=0)
```

<br>

### **2. Modification du service**

<br>

🌞 Identifier le fichier de configuration du serveur SSH

```bash
[lm33@machine1 ssh]$ ls -a
.       ssh_config    sshd_config.d           ssh_host_ed25519_key      ssh_host_rsa_key.pub
..      ssh_config.d  ssh_host_ecdsa_key      ssh_host_ed25519_key.pub
```

Le fichier de configuration du serveur SSH est sshd_config.

🌞 Modifier le fichier de conf

```bash
[lm33@machine1 ssh]$ echo $RANDOM
15835
```

Avant :
```bash
[lm33@machine1 ssh]$ sudo cat sshd_config | grep -i port
# If you want to change the port on a SELinux system, you have to tell
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#Port 22
# WARNING: 'UsePAM no' is not supported in Fedora and may cause several
#GatewayPorts no
```
Après:
```bash
[lm33@machine1 ssh]$ sudo cat sshd_config | grep -i port
# If you want to change the port on a SELinux system, you have to tell
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
Port 15835
# WARNING: 'UsePAM no' is not supported in Fedora and may cause several
#GatewayPorts no
```

```bash
[lm33@machine1 ssh]$ sudo firewall-cmd --remove-port=22/tcp --permanent
Warning: NOT_ENABLED: 22:tcp
success
[lm33@machine1 ssh]$ sudo firewall-cmd --add-port=15835/tcp --permanent
success
[lm33@machine1 ssh]$ sudo firewall-cmd --reload
success
[lm33@machine1 ssh]$ sudo firewall-cmd --list-all | grep -i port
  ports: 15835/tcp
  forward-ports:
  source-ports:
```

🌞 Redémarrer le service

```bash
systemctl restart sshd
```

🌞 Effectuer une connexion SSH sur le nouveau port

```bash
PS C:\Users\macel> ssh lm33@10.3.1.24 -p 15835
lm33@10.3.1.24's password:
Last login: Mon Dec  5 12:07:33 2022 from 10.3.1.3
[lm33@machine1 ~]$
```

<br>

## **II. Service HTTP**

<br>

### **1. Mise en place**

<br>

🌞 Installer le serveur NGINX

```bash
[lm33@machine1 ssh]$ sudo dnf install nginx
[sudo] password for lm33:
Last metadata expiration check: 0:07:41 ago on Mon 05 Dec 2022 12:15:39 PM CET.
Dependencies resolved.
======================================================================================================
 Package                      Architecture      Version                    Repository            Size
======================================================================================================
Installing:
 nginx                        x86_64            1:1.20.1-13.el9            appstream             38 k
Installing dependencies:
 nginx-core                   x86_64            1:1.20.1-13.el9            appstream            567 k
 nginx-filesystem             noarch            1:1.20.1-13.el9            appstream             11 k
 rocky-logos-httpd            noarch            90.13-1.el9                appstream             24 k

Transaction Summary
======================================================================================================
Install  4 Packages

Total download size: 640 k
Installed size: 1.8 M
Is this ok [y/N]: y
Downloading Packages:
(1/4): nginx-filesystem-1.20.1-13.el9.noarch.rpm                      103 kB/s |  11 kB     00:00
(2/4): rocky-logos-httpd-90.13-1.el9.noarch.rpm                       200 kB/s |  24 kB     00:00
(3/4): nginx-1.20.1-13.el9.x86_64.rpm                                 308 kB/s |  38 kB     00:00
(4/4): nginx-core-1.20.1-13.el9.x86_64.rpm                            3.0 MB/s | 567 kB     00:00
------------------------------------------------------------------------------------------------------
Total                                                                 1.1 MB/s | 640 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                              1/1
  Running scriptlet: nginx-filesystem-1:1.20.1-13.el9.noarch                                      1/4
  Installing       : nginx-filesystem-1:1.20.1-13.el9.noarch                                      1/4
  Installing       : nginx-core-1:1.20.1-13.el9.x86_64                                            2/4
  Installing       : rocky-logos-httpd-90.13-1.el9.noarch                                         3/4
  Installing       : nginx-1:1.20.1-13.el9.x86_64                                                 4/4
  Running scriptlet: nginx-1:1.20.1-13.el9.x86_64                                                 4/4
  Verifying        : rocky-logos-httpd-90.13-1.el9.noarch                                         1/4
  Verifying        : nginx-filesystem-1:1.20.1-13.el9.noarch                                      2/4
  Verifying        : nginx-1:1.20.1-13.el9.x86_64                                                 3/4
  Verifying        : nginx-core-1:1.20.1-13.el9.x86_64                                            4/4

Installed:
  nginx-1:1.20.1-13.el9.x86_64                        nginx-core-1:1.20.1-13.el9.x86_64
  nginx-filesystem-1:1.20.1-13.el9.noarch             rocky-logos-httpd-90.13-1.el9.noarch

Complete!
```

🌞 Démarrer le service NGINX

```bash
[lm33@machine1 ssh]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[lm33@machine1 ssh]$ sudo systemctl start nginx
```

🌞 Déterminer sur quel port tourne NGINX

```bash
[lm33@machine1 ssh]$ sudo ss -alnpt | grep nginx
[sudo] password for lm33:
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=11068,fd=6),("nginx",pid=11067,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=11068,fd=7),("nginx",pid=11067,fd=7))
```

```bash
[lm33@machine1 ssh]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[lm33@machine1 ssh]$ sudo firewall-cmd --reload
success
[lm33@machine1 ssh]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 Déterminer les processus liés à l'exécution de NGINX

```bash
[lm33@machine1 ~]$ ps -ef | grep nginx
root         819       1  0 10:21 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx        822     819  0 10:21 ?        00:00:00 nginx: worker process
lm33         932     909  0 10:23 pts/0    00:00:00 grep --color=auto nginx
```

🌞 Euh wait


```bash
macel@LAPTOP-EV3OHFA1 MINGW64 ~
$ curl 10.3.1.24:80 | head -7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  4120k      0 --:--:-- --:--:-- --:--:-- 7441k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

<br>

### **2. Analyser la conf de NGINX**

<br>

🌞 Déterminer le path du fichier de configuration de NGINX

```bash
[lm33@machine1 nginx]$ cat nginx.conf | grep "server {" -A 19
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

```bash
[lm33@machine1 nginx]$ cat nginx.conf | grep include
include /etc/nginx/conf.d/*.conf;
```

<br>

### **3. Déployer un nouveau site web**

<br>

🌞 Créer un site web

```bash
[lm33@machine1 nginx]$ cat /var/www/tp2_linux/index.html
<h1>MEOW mon premier serveur web</h1>
```

🌞 Adapter la conf NGINX

```bash
[lm33@machine1 conf.d]$ cat monfichierconfavecunnombienlong.conf
server {
  listen 7470;

  root /var/www/tp2_linux;
}
[lm33@machine1 conf.d]$ sudo systemctl restart nginx
[lm33@machine1 conf.d]$ sudo ss -lapte | grep nginx
LISTEN 0      511          0.0.0.0:7470      0.0.0.0:*     users:(("nginx",pid=1083,fd=6),("nginx",pid=1082,fd=6)) ino:21722 sk:1 cgroup:/system.slice/nginx.service <->
[lm33@machine1 conf.d]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[lm33@machine1 conf.d]$ sudo firewall-cmd --add-port=7470/tcp --permanent
success
[lm33@machine1 conf.d]$ sudo firewall-cmd --reload
success
[lm33@machine1 conf.d]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp 7470/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 Visitez votre super site web

```bah
macel@LAPTOP-EV3OHFA1 MINGW64 ~
$ curl 10.3.1.24:7470
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    38  100    38    0     0  28127      0 --:--:-- --:--:-- --:--:-- 38000<h1>MEOW mon premier serveur web</h1>
```

## **III. Your own services**

### **2. Analyse des services existants**

🌞 Afficher le fichier de service SSH

```bash
[lm33@machine1 /]$ systemctl status sshd
● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
--
[lm33@machine1 /]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart
ExecStart=/usr/sbin/sshd -D $OPTIONS

```

🌞 Afficher le fichier de service NGINX

```bash
[lm33@machine1 /]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
[lm33@machine1 /]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

<br>

### **3. Création de service**

<br>

🌞 Créez le fichier /etc/systemd/system/tp2_nc.service

```bash
[lm33@machine1 /]$ sudo cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 12651
```

🌞 Indiquer au système qu'on a modifié les fichiers de service

```bash
[lm33@machine1 /]$ sudo systemctl daemon-reload
```

🌞 Démarrer notre service de ouf

```bash
[lm33@machine1 /]$ sudo systemctl start tp2_nc
```

🌞 Vérifier que ça fonctionne

```bash
[lm33@machine1 /]$ systemctl status tp2_nc
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Tue 2022-12-06 11:43:12 CET; 14s ago
   Main PID: 1271 (nc)
      Tasks: 1 (limit: 5906)
     Memory: 776.0K
        CPU: 2ms
     CGroup: /system.slice/tp2_nc.service
             └─1271 /usr/bin/nc -l 12651

Dec 06 11:43:12 machine1 systemd[1]: Started Super netcat tout fou.
```

```bash
[lm33@machine1 /]$ sudo ss -lapnt | grep nc
LISTEN 0      10           0.0.0.0:12651      0.0.0.0:*     users:(("nc",pid=1271,fd=4))

LISTEN 0      10              [::]:12651         [::]:*     users:(("nc",pid=1271,fd=3))
```

```bash
[lm33@netcatcomm ~]$ nc 10.3.1.24 12651
gh
hello on test le neetcat
```

🌞 Les logs de votre service

```bash
[lm33@machine1 /]$ sudo journalctl -xe -u tp2_nc | grep -i start
Dec 06 12:00:23 machine1 systemd[1]: Started Super netcat tout fou.
░░ Subject: A start job for unit tp2_nc.service has finished successfully
░░ A start job for unit tp2_nc.service has finished successfully.
```

```bash
[lm33@machine1 /]$ sudo journalctl -xe -u tp2_nc | grep -i 1304
Dec 06 12:00:44 machine1 nc[1304]: gh
Dec 06 12:01:01 machine1 nc[1304]: hello on test le neetcat
Dec 06 12:03:40 machine1 nc[1304]: Oh mais ça marche
Dec 06 12:03:45 machine1 nc[1304]: C'est cool ça
```

```bash
[lm33@machine1 /]$ sudo journalctl -xe -u tp2_nc | grep -i deactivated
Dec 06 12:05:44 machine1 systemd[1]: tp2_nc.service: Deactivated successfully.
```

🌞 Affiner la définition du service

```bash
[lm33@machine1 /]$ cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 12651
Restart=always
```