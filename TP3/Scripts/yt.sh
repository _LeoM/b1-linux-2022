#!/bin/bash

if [[ $(find /var/log/yt/ 2> /dev/null) ]]
then
  :
else
  echo "Log file (/var/log/yt) didn't existe."
  exit
fi
if [[ $(find /srv/yt/downloads 2>  /dev/null) ]]
then
  :
else
  exit
fi

LINK=$(echo "${1}")
VIDEONAME=$(youtube-dl -e "${LINK}")
VIDEODESC=$(youtube-dl --get-description "${LINK}")

mkdir -p ./downloads/"${VIDEONAME}"
echo "${VIDEODESC}" > ./downloads/"${VIDEONAME}"/description

cd ./downloads/"${VIDEONAME}"
youtube-dl "${LINK}" > /dev/null
cd ../..

FILEPATH="/srv/yt/downloads/"${VIDEONAME}/"$(youtube-dl "${LINK}" --get-filename)"

$(echo "[$(date "+%y/%m/%d %H:%M:%S")] Video "${LINK}" was downloaded. File path : "${FILEPATH}"" >> /var/log/yt/download.log)

echo "Video ${LINK} was downloaded.
File path : "${FILEPATH}""
