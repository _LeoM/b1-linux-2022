#!/bin/bash
source /etc/os-release
echo "Machine name :$(hostnamectl | grep -i "static hostname" | cut -d':' -f2 )
OS ${NAME} and kernel version is $(uname -r)
IP : $(ip a | grep inet | grep global | tr -s ' ' | cut -d' ' -f3 | head -n1)
RAM : $(free -h | tr -s ' ' | cut -d' ' -f4 | head -n2 | tail -n1) memory available on $(free -h | tr -s ' ' | cut -d' ' -f2 | head -n2 | tail -n1) total memory
Disk : $(df -h | grep /dev/mapper/rl-root |tr -s ' ' | cut -d' ' -f 2) space left
Top 5 processes by RAM usage :"
for n in {1..5}
do
  echo - $(ps aux | sort -rnk 4 | head -n${n} | tail -n1 | tr -s ' ' | cut -d' ' -f11-)
done

echo "Listening ports :"
while read var; 
do
  port=$(echo "${var}" | cut -d" " -f5 | cut -d":" -f2)
  protocol=$(echo "${var}" | cut -d" " -f1)
  process=$(echo "${var}" | cut -d'"' -f2)
  echo "  - ${port} ${protocol} : ${process}"
done <<< $(ss -ltupn4H | tr -s ' ')  

curl https://cataas.com/cat > cat.jpg

echo "Here is your random cat : ./cat.jpg\n"