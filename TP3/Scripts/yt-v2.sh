#!/bin/bash

if [[ $(find /var/log/yt/ 2> /dev/null) ]]
then
  :
else
  echo "Log file (/var/log/yt) didn't existe."
  exit
fi
if [[ $(find /srv/yt/downloads 2>  /dev/null) ]]
then
  :
else
  exit
fi

LINKFILE="/srv/yt/link"

while :
do
  if [[ -s "${LINKFILE}" ]] # Check if file is empty
  then # The file isn't empty
    LINK=$(head -n1 "${LINKFILE}")
    if [[ $(youtube-dl -s "${LINK}" 2> /dev/null) ]]
    then
      VIDEONAME=$(youtube-dl -e "${LINK}")
      VIDEODESC=$(youtube-dl --get-description "${LINK}")

      mkdir -p /srv/yt/downloads/"${VIDEONAME}"
      echo "${VIDEODESC}" > /srv/yt/downloads/"${VIDEONAME}"/description

      cd /srv/yt/downloads/"${VIDEONAME}"
      youtube-dl "${LINK}" > /dev/null
      cd /srv/yt

      FILEPATH="/srv/yt/downloads/"${VIDEONAME}/"$(youtube-dl "${LINK}" --get-filename)"

      $(echo "[$(date "+%y/%m/%d %H:%M:%S")] Video "${LINK}" was downloaded. File path : "${FILEPATH}"" >> /var/log/yt/download.log)

      echo "Video ${LINK} was downloaded.
      File path : "${FILEPATH}""
    else
      $(echo "[$(date "+%y/%m/%d %H:%M:%S")] Link : "${LINK}" isn't valid"  >> /var/log/yt/download.log)
    fi
    sed -i '1d' "${LINKFILE}"
  fi
  sleep 1
done


