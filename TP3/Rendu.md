<br>

# **TP 3 : We do a little scripting**

<br>

## **I. Script carte d'identité**

<br>

📁 [Script IdCard](/TP3/Scripts/idcard.sh)

```bash
[lm33@machineCours idcard]$ sudo bash idcard.sh
Machine name :  machineCours
OS Rocky Linux and kernel version is 5.14.0-70.26.1.el9_0.x86_64
IP : 10.0.2.15/24
RAM : 700Mi memory available on 960Mi total memory
Disk : 6.3G space left
Top 5 processes by RAM usage :
- /usr/bin/python3 -s /usr/sbin/firewalld --nofork --nopid
- /usr/sbin/NetworkManager --no-daemon
- /usr/lib/systemd/systemd --switched-root --system --deserialize 28
- /usr/lib/systemd/systemd --user
- /usr/lib/systemd/systemd-logind
Listening ports :
  - 323 udp : chronyd
  - 80 tcp : nginx
  - 22 tcp : sshd
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 14815  100 14815    0     0  34941      0 --:--:-- --:--:-- --:--:-- 34858
Here is your random cat : ./cat.jpg
```

<br>

## **II. Script youtube-dl**

<br>

📁 [Script Youtube](/TP3/Scripts/yt.sh)

📁 [Log](/TP3/Scripts/download.log)

```bash
[lm33@machineCours yt]$ bash yt.sh https://www.youtube.com/watch?v=cdwal5Kw3Fc
Video https://www.youtube.com/watch?v=cdwal5Kw3Fc was downloaded.
File path : /srv/yt/downloads/World's Shortest YouTube Video! (0.00000000001 sec.)/World's Shortest YouTube Video! (0.00000000001 sec.)-cdwal5Kw3Fc.mp4
```

## **III. MAKE IT A SERVICE**

📁 [Script Youtube-V2](/TP3/Scripts/yt-v2.sh)

📁 [Servie](/TP3/Scripts/yt.service)


```bash
[lm33@machineCours system]$ systemctl status yt
● yt.service - Download Youtube videos with their link listed in the file /srv/yt/link
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-12-12 22:44:18 CET; 42s ago
   Main PID: 3576 (bash)
      Tasks: 2 (limit: 5906)
     Memory: 620.0K
        CPU: 11.216s
     CGroup: /system.slice/yt.service
             ├─3576 /bin/bash /srv/yt/yt-v2.sh
             └─3628 sleep 1

Dec 12 22:44:18 machineCours systemd[1]: Started Download Youtube videos with their link listed in the file /srv/yt/link.
```

```bash
Dec 12 22:44:18 machineCours systemd[1]: Started Download Youtube videos with their link listed in the file /srv/yt/link.
░░ Subject: A start job for unit yt.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit yt.service has finished successfully.
░░
░░ The job identifier is 1938.
Dec 12 22:44:53 machineCours bash[3576]: Video https://www.youtube.com/watch?v=jhFDyDgMVUI was downloaded.
Dec 12 22:44:53 machineCours bash[3576]:       File path : /srv/yt/downloads/One Second Video/One Second Video-jhFDyDgMVUI.mp4
```