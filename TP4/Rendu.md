
# **TP4 : Real services**

<br>

## **Partie 1 : Partitionnement du serveur de stockage**

🌞 Partitionner le disque à l'aide de LVM

```bash
[lm33@stockage ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0  8.1G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0  7.1G  0 part
  ├─rl-root 253:0    0  6.3G  0 lvm  /
  └─rl-swap 253:1    0  832M  0 lvm  [SWAP]
sdb           8:16   0    2G  0 disk
sr0          11:0    1 1024M  0 rom
[lm33@stockage ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[lm33@stockage ~]$ sudo pvs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB062a22e9-de6c1373_ PVID svB98WIdOdU85wMppVfJxn0koYfDOdvE last seen on /dev/sda2 not found.
  PV         VG Fmt  Attr PSize PFree
  /dev/sdb      lvm2 ---  2.00g 2.00g
[lm33@stockage ~]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created
[lm33@stockage ~]$ sudo vgs
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB062a22e9-de6c1373_ PVID svB98WIdOdU85wMppVfJxn0koYfDOdvE last seen on /dev/sda2 not found.
  VG      #PV #LV #SN Attr   VSize  VFree
  storage   1   0   0 wz--n- <2.00g <2.00g
[lm33@stockage ~]$ sudo lvcreate -l 100%FREE storage -n lvstorage
  Logical volume "lvstorage" created.
```

🌞 Formater la partition

```bash
[lm33@stockage ~]$ sudo mkfs -t ext4 /dev/storage/lvstorage
mke2fs 1.46.5 (30-Dec-2021)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: bfc97293-685c-436e-97d5-0626b8180c12
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
[lm33@stockage ~]$ sudo lvdisplay
  Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VB062a22e9-de6c1373_ PVID svB98WIdOdU85wMppVfJxn0koYfDOdvE last seen on /dev/sda2 not found.
  --- Logical volume ---
  LV Path                /dev/storage/lvstorage
  LV Name                lvstorage
  VG Name                storage
  LV UUID                YgKKA3-ncdi-rI8A-juLx-mWc4-pJd1-fUPC0z
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2022-12-13 11:04:07 +0100
  LV Status              available
  # open                 1
  LV Size                <2.00 GiB
  Current LE             511
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
```

🌞 Monter la partition

```bash
[lm33@stockage ~]$ sudo mkdir /mnt/storage
[lm33@stockage ~]$ ls /mnt
storage
[lm33@stockage ~]$ sudo mount /dev/storage/lvstorage /mnt/storage/
[lm33@stockage ~]$ df -h | grep storage
/dev/mapper/storage-lvstorage  2.0G   24K  1.9G   1% /mnt/storage
[lm33@stockage ~]$ sudo nano /mnt/storage/test
[lm33@stockage ~]$ sudo cat /mnt/storage/test
I can write and read
```

```bash
[lm33@stockage ~]$ sudo nano /etc/fstab
[lm33@stockage ~]$ cat /etc/fstab
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=21594b90-fb3a-4148-880f-f5be4a195883 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/storage/lvstorage /mnt/storage ext4 defaults 0 0
```

```bash
[lm33@stockage ~]$ sudo umount /mnt/storage
[lm33@stockage ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/storage does not contain SELinux labels.
       You just mounted a file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/storage             : successfully mounted
```

<br>

## **Partie 2 : Serveur de partage de fichiers**

🌞 Donnez les commandes réalisées sur le serveur NFS storage.tp4.linux


```bash
[lm33@stockage ~]$ sudo dnf install nfs-utils
...
Complete!

[lm33@stockage ~]$ sudo mkdir /storage/site_web_1/ -p
[lm33@stockage ~]$ sudo chown nobody /storage/site_web_1/
[lm33@stockage ~]$ sudo mkdir /storage/site_web_2/ -p
[lm33@stockage ~]$ sudo chown nobody /storage/site_web_2/
[lm33@stockage ~]$ ls -dl /storage/site_web_1/
drwxr-xr-x. 2 nobody root 6 Dec 13 11:55 /storage/site_web_1/
[lm33@stockage ~]$ ls -dl /storage/site_web_2/
drwxr-xr-x. 2 nobody root 6 Dec 13 11:58 /storage/site_web_2/

[lm33@stockage ~]$ sudo dnf install nano

[lm33@stockage ~]$ sudo nano /etc/exports
[lm33@stockage ~]$ cat /etc/exports
/storage/site_web_1/    10.4.1.64(rw,sync,no_subtree_check)
/storage/site_web_2/    10.4.1.64(rw,sync,no_subtree_check)

[lm33@stockage ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[lm33@stockage ~]$ sudo systemctl start nfs-server
[lm33@stockage ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor preset: disabled)
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Tue 2022-12-13 12:02:28 CET; 7s ago
    Process: 11322 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
    Process: 11323 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 11340 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gss>
   Main PID: 11340 (code=exited, status=0/SUCCESS)
        CPU: 17ms

Dec 13 12:02:28 stockage systemd[1]: Starting NFS server and services...
Dec 13 12:02:28 stockage systemd[1]: Finished NFS server and services.

[lm33@stockage ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client ssh
[lm33@stockage ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[lm33@stockage ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[lm33@stockage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[lm33@stockage ~]$ sudo firewall-cmd --reload
success
[lm33@stockage ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client mountd nfs rpc-bind ssh

[lm33@stockage ~]$ cat /etc/exports
/storage/site_web_1/    10.4.1.64(rw,sync,no_subtree_check)
/storage/site_web_2/    10.4.1.64(rw,sync,no_subtree_check)
```

🌞 Donnez les commandes réalisées sur le client NFS web.tp4.linux

```bash
[lm33@webserv ~]$ sudo dnf install nfs-utils
...
Complete!

[lm33@webserv ~]$ sudo mkdir -p /var/www/site_web_1/
[lm33@webserv ~]$ sudo mkdir -p /var/www/site_web_2/
[lm33@webserv ~]$ sudo mount 10.4.1.54:/storage/site_web_1/ /var/www/site_web_1/
[lm33@webserv ~]$ sudo mount 10.4.1.54:/storage/site_web_2/ /var/www/site_web_2/
[lm33@webserv ~]$ df -h
Filesystem                     Size  Used Avail Use% Mounted on
devtmpfs                       462M     0  462M   0% /dev
tmpfs                          481M     0  481M   0% /dev/shm
tmpfs                          193M  5.2M  187M   3% /run
/dev/mapper/rl-root            6.3G  1.2G  5.2G  18% /
/dev/sda1                     1014M  210M  805M  21% /boot
tmpfs                           97M     0   97M   0% /run/user/1000
10.4.1.54:/storage/site_web_1  6.3G  1.2G  5.2G  18% /var/www/site_web_1
10.4.1.54:/storage/site_web_2  6.3G  1.2G  5.2G  18% /var/www/site_web_2

[lm33@webserv ~]$ sudo nano /etc/fstab
[lm33@webserv ~]$ cat /etc/fstab
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=21594b90-fb3a-4148-880f-f5be4a195883 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.4.1.54:/storage/site_web_1/  /var/www/site_web_1/    nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0

10.4.1.54:/storage/site_web_2/  /var/www/site_web_2/    nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```


## **Partie 3 : Serveur web**

### **2. Install**

🌞 Installez NGINX


```bash
[lm33@webserv ~]$ sudo dnf install nginx
...
Complete!
```

### **3. Analyse**

🌞 Analysez le service NGINX

```bash
[lm33@webserv ~]$ ps -ef | grep nginx
root        1136       1  0 12:39 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1137    1136  0 12:39 ?        00:00:00 nginx: worker process
```

```bash
[lm33@webserv ~]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1137,fd=6),("nginx",pid=1136,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1137,fd=7),("nginx",pid=1136,fd=7))
```

```bash
[lm33@webserv nginx]$ cat nginx.conf | grep root
        root         /usr/share/nginx/html;
```

```bash
[lm33@web nginx]$ ls -l /usr/share/nginx/html
total 12
-rw-r--r--. 1 root root 3332 Oct 31 16:35 404.html
-rw-r--r--. 1 root root 3404 Oct 31 16:35 50x.html
drwxr-xr-x. 2 root root   27 Dec 13 12:38 icons
lrwxrwxrwx. 1 root root   25 Oct 31 16:37 index.html -> ../../testpage/index.html
-rw-r--r--. 1 root root  368 Oct 31 16:35 nginx-logo.png
lrwxrwxrwx. 1 root root   14 Oct 31 16:37 poweredby.png -> nginx-logo.png
lrwxrwxrwx. 1 root root   37 Oct 31 16:37 system_noindex_logo.png -> ../../pixmaps/system-noindex-logo.png
```

### **4. Visite du service web**

🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX

```bash
[lm33@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for lm33:
success
[lm33@web ~]$ sudo firewall-cmd --reload
success
[lm33@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 Accéder au site web

```bash
PS C:\Users\macel> curl 10.4.1.64:80

StatusCode        : 200
StatusDescription : OK
Content           : <!doctype html>
                    <html>
                      <head>
                        <meta charset='utf-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1'>
                        <title>HTTP Server Test Page powered by: Rocky Linux</title>
                       ...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 7620
                    Content-Type: text/html
                    Date: Mon, 02 Jan 2023 08:46:21 GMT
                    ETag: "62e17e64-1dc4"
                    Last-Modified: Wed, 27 Jul 202...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 7620],
                    [Content-Type, text/html]...}
Images            : {@{innerHTML=; innerText=; outerHTML=<IMG alt="[ Powered by Rocky Linux ]"
                    src="icons/poweredby.png">; outerText=; tagName=IMG; alt=[ Powered by Rocky
                    Linux ]; src=icons/poweredby.png}, @{innerHTML=; innerText=; outerHTML=<IMG
                    src="poweredby.png">; outerText=; tagName=IMG; src=poweredby.png}}
InputFields       : {}
Links             : {@{innerHTML=<STRONG>Rocky Linux website</STRONG>; innerText=Rocky Linux
                    website; outerHTML=<A href="https://rockylinux.org/"><STRONG>Rocky Linux
                    website</STRONG></A>; outerText=Rocky Linux website; tagName=A;
                    href=https://rockylinux.org/}, @{innerHTML=Apache Webserver</STRONG>;
                    innerText=Apache Webserver; outerHTML=<A href="https://httpd.apache.org/">Apache
                    Webserver</STRONG></A>; outerText=Apache Webserver; tagName=A;
                    href=https://httpd.apache.org/}, @{innerHTML=Nginx</STRONG>; innerText=Nginx;
                    outerHTML=<A href="https://nginx.org">Nginx</STRONG></A>; outerText=Nginx;
                    tagName=A; href=https://nginx.org}, @{innerHTML=<IMG alt="[ Powered by Rocky
                    Linux ]" src="icons/poweredby.png">; innerText=; outerHTML=<A id=rocky-poweredby
                    href="https://rockylinux.org/"><IMG alt="[ Powered by Rocky Linux ]"
                    src="icons/poweredby.png"></A>; outerText=; tagName=A; id=rocky-poweredby;
                    href=https://rockylinux.org/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 7620
```

🌞 Vérifier les logs d'accès

```bash
[lm33@web log]$ sudo tail -3 nginx/access.log
10.4.1.1 - - [02/Jan/2023:09:26:04 +0100] "GET /poweredby.png HTTP/1.1" 200 368 "http://10.4.1.64/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 OPR/93.0.0.0" "-"
10.4.1.1 - - [02/Jan/2023:09:26:04 +0100] "GET /favicon.ico HTTP/1.1" 404 3332 "http://10.4.1.64/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 OPR/93.0.0.0" "-"
10.4.1.1 - - [02/Jan/2023:09:46:21 +0100] "GET / HTTP/1.1" 200 7620 "-" "Mozilla/5.0 (Windows NT; Windows NT 10.0; fr-FR) WindowsPowerShell/5.1.22621.963" "-"
```

### **5. Modif de la conf du serveur web**

🌞 Changer le port d'écoute

```bash
[lm33@web nginx]$ sudo cat nginx.conf | grep listen
        listen       8080;

```

```bash 
[lm33@web nginx]$ sudo systemctl restart nginx
[lm33@web nginx]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-02 10:00:23 CET; 7s ago
    Process: 1111 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1112 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1114 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1115 (nginx)
      Tasks: 2 (limit: 5906)
     Memory: 1.9M
        CPU: 16ms
     CGroup: /system.slice/nginx.service
             ├─1115 "nginx: master process /usr/sbin/nginx"
             └─1116 "nginx: worker process"

Jan 02 10:00:23 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jan 02 10:00:23 web nginx[1112]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Jan 02 10:00:23 web nginx[1112]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Jan 02 10:00:23 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```

```bash
[lm33@web nginx]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1116,fd=6),("nginx",pid=1115,fd=6))
```

```bash
[lm33@web nginx]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[lm33@web nginx]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[lm33@web nginx]$ sudo firewall-cmd --reload
success
[lm33@web nginx]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```bash
PS C:\Users\macel> curl 10.4.1.64:8080

StatusCode        : 200
StatusDescription : OK
Content           : <!doctype html>
                    <html>
                      <head>
                        <meta charset='utf-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1'>
                        <title>HTTP Server Test Page powered by: Rocky Linux</title>
                       ...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 7620
                    Content-Type: text/html
                    Date: Mon, 02 Jan 2023 09:12:05 GMT
                    ETag: "62e17e64-1dc4"
                    Last-Modified: Wed, 27 Jul 202...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 7620],
                    [Content-Type, text/html]...}
Images            : {@{innerHTML=; innerText=; outerHTML=<IMG alt="[ Powered by Rocky Linux ]"
                    src="icons/poweredby.png">; outerText=; tagName=IMG; alt=[ Powered by Rocky
                    Linux ]; src=icons/poweredby.png}, @{innerHTML=; innerText=; outerHTML=<IMG
                    src="poweredby.png">; outerText=; tagName=IMG; src=poweredby.png}}
InputFields       : {}
Links             : {@{innerHTML=<STRONG>Rocky Linux website</STRONG>; innerText=Rocky Linux
                    website; outerHTML=<A href="https://rockylinux.org/"><STRONG>Rocky Linux
                    website</STRONG></A>; outerText=Rocky Linux website; tagName=A;
                    href=https://rockylinux.org/}, @{innerHTML=Apache Webserver</STRONG>;
                    innerText=Apache Webserver; outerHTML=<A href="https://httpd.apache.org/">Apache
                    Webserver</STRONG></A>; outerText=Apache Webserver; tagName=A;
                    href=https://httpd.apache.org/}, @{innerHTML=Nginx</STRONG>; innerText=Nginx;
                    outerHTML=<A href="https://nginx.org">Nginx</STRONG></A>; outerText=Nginx;
                    tagName=A; href=https://nginx.org}, @{innerHTML=<IMG alt="[ Powered by Rocky
                    Linux ]" src="icons/poweredby.png">; innerText=; outerHTML=<A id=rocky-poweredby
                    href="https://rockylinux.org/"><IMG alt="[ Powered by Rocky Linux ]"
                    src="icons/poweredby.png"></A>; outerText=; tagName=A; id=rocky-poweredby;
                    href=https://rockylinux.org/}...}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 7620
```

🌞 Changer l'utilisateur qui lance le service

```bash
[lm33@web home]$ sudo useradd web -m -p l
```

```bash
[lm33@web nginx]$ cat nginx.conf | grep user
user web;
```

```bash
[lm33@web nginx]$ ps -ef | grep nginx
root        1241       1  0 10:29 ?        00:00:00 nginx: master process /usr/sbin/nginx
web         1242    1241  0 10:29 ?        00:00:00 nginx: worker process
lm33        1254     928  0 10:32 pts/0    00:00:00 grep --color=auto nginx
```

🌞 Changer l'emplacement de la racine Web

```bash
[lm33@web nginx]$ sudo nano /var/www/site_web_1/index.html
[lm33@web nginx]$ sudo cat /var/www/site_web_1/index.html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Site 1</title>
</head>
<body>
    <h1>Hello first Site</h1>
</body>
</html>
```

```bash
[lm33@web nginx]$ sudo nano nginx.conf
[lm33@web nginx]$ sudo cat nginx.conf | grep root
        root         /var/www/site_web_1;
[lm33@web nginx]$ sudo systemctl restart nginx
[lm33@web nginx]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2023-01-02 10:36:38 CET; 3s ago
    Process: 1270 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 1272 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 1274 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 1275 (nginx)
      Tasks: 2 (limit: 5906)
     Memory: 1.9M
        CPU: 15ms
     CGroup: /system.slice/nginx.service
             ├─1275 "nginx: master process /usr/sbin/nginx"
             └─1276 "nginx: worker process"

Jan 02 10:36:38 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Jan 02 10:36:38 web nginx[1272]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Jan 02 10:36:38 web nginx[1272]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Jan 02 10:36:38 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```

```bash
PS C:\Users\macel> curl 10.4.1.64:8080

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    ...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 287
                    Content-Type: text/html
                    Date: Mon, 02 Jan 2023 09:39:44 GMT
                    ETag: "63b2a567-11f"
                    Last-Modified: Mon, 02 Jan 2023 ...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 287],
                    [Content-Type, text/html]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 287
```

### **6. Deux sites web sur un seul serveur**

🌞 Repérez dans le fichier de conf

```bash
[lm33@web nginx]$ sudo cat nginx.conf | grep include.*conf.d
    include /etc/nginx/conf.d/*.conf;
```

🌞 Créez le fichier de configuration pour le premier site

```bash
[lm33@web nginx]$ sudo cat conf.d/site_web_1.conf
server {
        listen       8080;
        server_name  _;
        root         /var/www/site_web_1;
        include /etc/nginx/default.d/*.conf;
        error_page 404 /404.html;
        location = /404.html {
        }
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
}
```

🌞 Créez le fichier de configuration pour le deuxième site

```bash
[lm33@web nginx]$ sudo cat conf.d/site_web_2.conf
server {
        listen       8888;
        server_name  _;
        root         /var/www/site_web_2;
        include /etc/nginx/default.d/*.conf;
        error_page 404 /404.html;
        location = /404.html {
        }
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
}
```

```bash
[lm33@web nginx]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[lm33@web nginx]$ sudo firewall-cmd --reload
success
[lm33@web nginx]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp 8888/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

```bash
[lm33@web nginx]$ sudo ss -alnpt | grep nginx
LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1415,fd=6),("nginx",pid=1414,fd=6))
LISTEN 0      511          0.0.0.0:8888      0.0.0.0:*    users:(("nginx",pid=1415,fd=7),("nginx",pid=1414,fd=7))
```

🌞 Prouvez que les deux sites sont disponibles

```bash
PS C:\Users\macel> curl 10.4.1.64:8888

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    ...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 289
                    Content-Type: text/html
                    Date: Mon, 02 Jan 2023 10:15:05 GMT
                    ETag: "63b2abe7-121"
                    Last-Modified: Mon, 02 Jan 2023 ...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 289],
                    [Content-Type, text/html]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 289



PS C:\Users\macel> curl 10.4.1.64:8080

StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    ...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Accept-Ranges: bytes
                    Content-Length: 287
                    Content-Type: text/html
                    Date: Mon, 02 Jan 2023 10:15:09 GMT
                    ETag: "63b2a567-11f"
                    Last-Modified: Mon, 02 Jan 2023 ...
Forms             : {}
Headers           : {[Connection, keep-alive], [Accept-Ranges, bytes], [Content-Length, 287],
                    [Content-Type, text/html]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 287
```